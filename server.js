const express = require("express");
const cors = require("cors");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const nodeRoute = require("./routes/nodeSetting.route");

require("dotenv").config();

const app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(cookieParser());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.use("/node", nodeRoute);

// app.use(errorHandler.generateAPIError)

const PORT = process.env.PORT || 5000;

//* Listening to the socket server instead express server
app.listen(PORT, () => {
  console.log(`Server running on http://localhost:${PORT}`);
});
