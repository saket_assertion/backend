const router = require("express").Router();
const node = require("../controllers/nodeSetting.controllers.js");

router.get("/nodes", node.getAllNodes); // List all Nodes from DB
router.post("/nodes", node.addNode); // Add a Node in a DB
router.get("/assets", node.getAssets); // list all of the assets attached to the node

module.exports = router;
