const low = require("lowdb");
const _ = require("lodash");
const FileSync = require("lowdb/adapters/FileSync");

require("dotenv").config();

let adapter = new FileSync("./db/database.json");
let db = low(adapter);

const defaultData = {
  nodes: [
    {
      node_name: "zone1",
      node_id: "dsd98s9d8f9s8d9832nkn",
      node_host: "172.16.23.9",
      node_region: "Asia/Pacific",
      node_country: "India",
      directory_config: {
        node_log_collection_basepath: "/siembridge/eventlog",
        node_log_archive_basepath: "/siembridge/archive/eventlog",
        core_log_upload_basepath: "/siembridge/nodes/zone1/eventlog",
        core_merged_log_basepath: "/siembridge/master/eventlog",
        core_merged_log_archive_basepath: "/siembridge/master/archive/eventlog",
        core_csv_collection_basepath: "/siembridge/master/csv",
        s3_csv_upload_basepath: "/siembridge/master/csv",
      },
      integrations: [
        {
          type: "ACM8",
          version: "8.0",
          syslog_port: "5140",
          assets: [
            {
              host: "172.16.2.32",
              user: "dadmin",
              password: "dadmin01",
              protocol: "ssh",
              port: "22",
            },
          ],
        },
        {
          type: "ACM7",
          version: "7.0",
          syslog_port: "514",
        },
        {
          type: "AES",
          version: "8.0",
          syslog_port: "5141",
        },
        {
          type: "AMG",
          version: "8.0",
          syslog_port: "514",
        },
      ],
      log_upload_server_details: {
        host: "3.17.89.211",
        user: "ubuntu",
        protocol: "scp",
        port: "22",
      },
    },
  ],
  cloud_api_details: {
    s3_upload_bucket: "SC-SIEM",
    s3_upload_base_path: "v1",
  },
};

db.defaults(defaultData).write();

module.exports = {
  // List all Nodes from DB
  getAllNodes:
    ("/nodes",
    async (req, res) => {
      try {
        const node = db.get("nodes").value();
        res.status(200).send(node);
      } catch (error) {
        res.send(error);
      }
    }),

  // Add a Node in a DB
  addNode:
    ("/nodes",
    async (req, res) => {
      try {
        let data = req.body;
        let deepCloneDefaultdata = _.cloneDeep(defaultData);

        let nodeCheck = db.get("nodes").find({ node_id: data.node_id }).value();

        if (_.isEmpty(nodeCheck)) {
          deepCloneDefaultdata.nodes[0].node_name = data.node_name;
          deepCloneDefaultdata.nodes[0].node_id = data.node_id;
          deepCloneDefaultdata.nodes[0].node_host = data.node_host;
          deepCloneDefaultdata.nodes[0].node_region = data.node_region;
          deepCloneDefaultdata.nodes[0].node_country = data.node_country;
          deepCloneDefaultdata.nodes[0].integrations[0].assets = data.assets;

          db.get("nodes").push(deepCloneDefaultdata.nodes[0]).write();

          const node = db.get("nodes").value();
          res.status(200).send(node);
        } else {
          res.status(200).send("Node already created");
        }
      } catch (error) {
        res.send("error");
      }
    }),

  // list all of the assets attached to the node
  getAssets:
    ("/assets",
    async (req, res) => {
      try {
        let nodeName = req.body.node_name;
        const assets = db.get("nodes").find({ node_name: nodeName }).value()
          .integrations[0].assets;

        res.status(200).send(assets);
      } catch (error) {
        res.send(error);
      }
    }),
};
